import 'package:flutter/material.dart';
import './quiz.dart';
import './result.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  final _questions = const [
    {
      'questionText': 'Siapa presiden pertama RI?',
      'answer': [
        {'text': 'Sukarno', 'score': 1},
        {'text': 'Suharto', 'score': 0},
        {'text': 'Megawati', 'score': 0},
        {'text': 'Habibie', 'score': 0},
      ],
    },
    {
      'questionText': 'Agama Konghucu disahkan ketika kepemimpinan presiden?',
      'answer': [
        {'text': 'Suharto', 'score': 0},
        {'text': 'Megawati', 'score': 0},
        {'text': 'Habibie', 'score': 0},
        {'text': 'Gus Dur', 'score': 1},
      ],
    },
    {
      'questionText': 'Ormas Islam terbesar di Indonesia?',
      'answer': [
        {'text': 'FPI', 'score': 0},
        {'text': 'Muhammadiyah', 'score': 0},
        {'text': 'Nahdlatul Ulama', 'score': 1},
        {'text': 'Nahdlatul Wathon', 'score': 0},
      ],
    },
  ];
  var _questionIndex = 0;
  var _totalScore = 0;

  void _resetQuiz() {
    setState(() {
     _questionIndex = 0;
     _totalScore = 0; 
    });
  }

  void _answerQuestion(int score) {
    // var aBool = true;
    // aBool = false;

    _totalScore += score;

    setState(() {
      _questionIndex = _questionIndex + 1;
    });
    print(_questionIndex);
    if (_questionIndex < _questions.length) {
      print('Pertanyaan Selanjutnya!');
    } else {
      print('Tidak Ke Pertanyaan Selanjutnya!');
    }
  }

  @override
  Widget build(BuildContext context) {
    // var dummy = const ['Hello'];
    // dummy.add('Max');
    // print(dummy);
    // dummy = [];

    // questions = [];

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Quiz cahya'),
        ),
        body: _questionIndex < _questions.length
            ? Quiz(
                answerQuestion: _answerQuestion,
                questionIndex: _questionIndex,
                questions: _questions,
              )
            : Result(_totalScore, _resetQuiz),
      ),
    );
  }
}
