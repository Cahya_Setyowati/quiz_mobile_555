import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultScore;
  final Function resetHandler;

  Result(this.resultScore, this.resetHandler);

  String get resultPhrase {
    String resultText;
    if (resultScore == 1) {
      resultText = 'Anda lulus dengan sempurna';
    } else if (resultScore == 2) {
      resultText = 'Anda Lulus dengan Baik';
    } else if (resultScore >= 3) {
      resultText = 'Anda Lulus dengan Biasa';
    } else {
      resultText = 'Anda Tidak Lulus (-_-)!';
    }
    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Nilai Anda :  ", style: TextStyle(fontSize: 26), ),
              Text(
                resultScore.toString(),
                style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold, color: Colors.blue),
              ),
            ],
          ),
          Text(
            resultPhrase,
            style: TextStyle(fontSize: 36, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
          FlatButton(
            child: Text('Restart Quiz!'
            ), 
            textColor: Colors.blue,
            onPressed: resetHandler,
          ),
        ],
      ),
    );
  }
}
